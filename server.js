require('newrelic');

import dotenv   from 'dotenv';
import express  from 'express';
import morgan   from 'morgan';
import path     from 'path';
import exphbs   from 'express-handlebars';

// import .env const
dotenv.config();

// static file dist directory
const dist_path = path.join(__dirname, 'dist');


// load assets data from json file
const assets = require(dist_path + '/assets.json');

// create server app
const app = express();

// set port
app.set('port', (process.env.PORT || 80));

// set server log output
app.use(morgan('combined'));

// configure handlebars
app.set('views', path.join(__dirname, 'views'));

app.engine('handlebars', exphbs({
  layoutsDir: path.join(__dirname, 'views'),
  partialsDir: path.join(__dirname, 'views')
}));

// set template engine
app.set('view engine', 'handlebars');

// set static path
app.use('/src/', express.static(path.join(__dirname, 'src')))
app.use('/dist/', express.static(path.join(__dirname, 'dist')))

// catch all route for React routes
app.get('/*', function (req, res) {
  res.render('app', {
    layout: false,
    dev: process.env.NODE_ENV == "dev" ? true : false,
    prod: process.env.NODE_ENV == "dev" ? false : true,
    js_file: assets.main ? assets.main.js : "",
    css_file: assets.main ? assets.main.css : ""
  });
})

// start server
app.listen(app.get('port'), function () {
  console.log('Server ready and listening on : ' + app.get('port'))
})
