import domready from 'domready';
import inViewport from 'in-viewport';

// import markdown file, webpack loader will convert file to HTML
const documentationHTML = require("../markdown/documentation.md");

// selectors
const documentationContainer = "#main-documentation";
const navContainer = "#nav-documentation > .menu-list";


/*
 * Main app
 */
class App {

  constructor() {
    this.init();
  }

  init() {
    return new Promise((resolve, reject) => {
      this.insertHTML();
      resolve();
    }).then(()=> {
      this.insertNav();
      this.embeded();
      $(window).on('scroll', this.spy);
    });
  }

  insertHTML() {
    $(documentationContainer).html(documentationHTML);
  }

  insertNav() {
    let $sections = $(documentationContainer).find('> h2');
    $sections.each((i, e) => {
      let $li = $('<li/>').appendTo( $(navContainer) );
      let domID  = $(e).attr('id');
      let $a  = $('<a/>')
        .attr('href', `#${domID}`)
        .text($(e).text())
        .appendTo($li);

        this.insertSubNav(e, $li)

      // attach element to section
      $(e).data('li-element', $li);
      $(e).data('a-element', $a);

      // continue loop
      return true;
    });
  }

  insertSubNav(section, $parentContainer) {
    let $subSections = $(section).nextUntil('h2').filter('h3');

    if ($subSections.length > 0) {
      let $ul = $('<ul/>').appendTo( $parentContainer ).addClass('foobar');
      $subSections.each((i, e) => {
        let $li = $('<li/>').appendTo( $ul );
        let domID  = $(e).attr('id');
        let $a  = $('<a/>')
          .attr('href', `#${domID}`)
          .text($(e).text())
          .appendTo($li);

        // continue loop
        return true;
      });
    }
  }

  embeded() {
    let $iframes = $(documentationContainer).find('iframe');
    $iframes.each((i, e) => {
      console.log(i, e);
      let $div = $("<div/>").addClass('responsive-embed').insertBefore(e);
      $(e).appendTo( $div );
    });
  }

  spy() {
    let $sections = $(documentationContainer).find('h2');
    $sections.each((i, e) => {
      if (inViewport(e)) {
        // reset others
        $(navContainer).find('li.is-active a').removeClass('is-active');
        $(navContainer).find('li.is-active').removeClass('is-active');

        // set target is-active
        $(e).data('li-element').addClass('is-active');
        $(e).data('a-element').addClass('is-active');
        return false;
      }
    });
  }

}


/*
 * Start app
 */
domready(() => {
  new App();
});
