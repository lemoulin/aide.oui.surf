## Introduction

Voici un guide d'utilisation de la plateforme WordPress de Oui.Surf.

http://oui.surf/wp-admin/

## Sections OuiSurf

Le site est séparé en 5 types de contenus, ceux-ci représentent les grandes sections du site :

* Articles
* Guides
* Dossiers
* Séries
* Vidéos

![](https://assets.oui.surf/wp-content/uploads/2017/10/03174627/skitch-13.png)

Tous les contenus sont ajoutés avec l'outil standard "Articles" de WordPress. À la sauvegarde vous devez sélectionner dans quelle(s) section(s) le contenu doit se retrouver.

![](https://assets.oui.surf/wp-content/uploads/2017/10/03174400/skitch-12.png)

**Note :** un contenu série peut être dans deux sections à la fois, lire plus bas dans la section *[séries](#s-ries-ouisurf)*.

![](https://assets.oui.surf/wp-content/uploads/2017/10/03174404/skitch-11.png)

## *Choix de l'équipe*

![](https://assets.oui.surf/wp-content/uploads/2017/10/03174624/skitch.png)

Pour ajouter du contenu dans les carousels *Choix de l'équipe* partout sur le site, cocher simplement l'option "_Sticky_" de WP. L'affichage des contenus est filtré selon le contexte. Par exemple, la section *Articles* affichera que des contenu de type articles, pas de contenus *guides* ou autres.

![](https://assets.oui.surf/wp-content/uploads/2017/10/03174539/skitch-2.png)

## Contenus en accueil

Le premier contenu visible en accueil est divisé en deux sections :

* **1** bannière promotionnel
* **2** contenus plus récents

Pour ajouter ou changer la bannière du haut, il faut créer un item dans la section [Sliders](https://oui.surf/wp-admin/edit.php?post_type=sliders) du CMS. Le lien peut pointer à un contenu interne ou vers un lien externe, sur Facebook par exemple.

Les deux contenus récents inclus tous les types de contenus (articles, dossiers, séries, guides, vidéos). L'option *sticky* de WP n'influence pas la position, seule la date de publication est prise en considération.

![](https://assets.oui.surf/wp-content/uploads/2017/10/10145614/skitch-18.png)

Pour placer du contenu dans la section vidéo, votre contenu doit absolument être classé *vidéos* même si c'est du contenu de *série*.

![](https://assets.oui.surf/wp-content/uploads/2017/10/03174404/skitch-11.png)

## Articles textes

Voici quelques elements de mises en page automatisé par le thème WP.

### Titres et paragraphes

Les textes de corps d'articles utilisent deux types de typographie: **sans-sérif** et **sérif**.

![](https://assets.oui.surf/wp-content/uploads/2017/10/10153021/skitch-22.png)

Par défault, tout est en sans-sérif. Sauf dans les dossiers où tout le contenu est affiché avec une police sérif (titres et paragraphes).

Il est aussi possible forcer le style de police avec l'éditeur texte avec le menu *"Formats"* dans la barre d'outils.

![](https://assets.oui.surf/wp-content/uploads/2017/10/10152927/skitch-21.png)


### Galeries d'images

Les galeries d'images standards de WP sont automatiquement converties dans un carousel horizontal une fois sur le site :

![](https://assets.oui.surf/wp-content/uploads/2017/10/03174415/skitch-10.png)

Ensuite sur le site :

![](https://assets.oui.surf/wp-content/uploads/2017/10/03174435/skitch-9.png)

### Image et vidéos en entête

L'image "_mise en avant_" du billet est toujours l'image qui sera affichée en entête du contenu. Par contre, vous pouvez remplacer cette image par un vidéo aussi, placez un fichier MP4 dans le champs spécial _Video header_ :

![](https://assets.oui.surf/wp-content/uploads/2017/10/03174534/skitch-3.png)

### Vidéos en lecture automatique

Vous pouvez inclure des vidéos en pleine largeur dans le texte avec l'aide de l'outil d'édition texte.

Article en exemple, et lien vers le formulaire d'édition du même article :

* https://oui.surf/les-vagues-artificielles/
* https://oui.surf/wp-admin/post.php?post=28910&action=edit

Dans l'éditeur texte, commencer par mettre en ligne un vidéo en format MP4.

1. Cliquer sur "Ajouter un média";
2. Envoyer votre fichier MP4 en premier ou sélectionner en un si déjà présent dans la bibliothèque médias;
3. Avant de cliquer sur "Insérer dans l'article", changer dans le menu déroulant à droite l'option d'intégration à "Lien vers la page du fichier.";
4. Cliquer sur "Insérer dans l'article".;

![](https://assets.oui.surf/wp-content/uploads/2017/10/03174443/skitch-8.png)

On devrait voir ceci ensuite dans l'éditeur texte WP, le thème transformera cette ligne en élément vidéo pleine largeur.

![](https://assets.oui.surf/wp-content/uploads/2017/10/03174446/skitch-7.png)

## Séries OuiSurf

Les séries utilisent un deuxième système de catégories, voici les étapes pour ajouter une série :

1. Allez ajouter la nouvelle [catégorie de série](https://oui.surf/wp-admin/edit-tags.php?taxonomy=series) avec l'aide du CMS;
2. Tous les champs sont importants, référez vous aux séries déjà en ligne pour le contenu à y mettre;
3. Ensuite vous pouvez créer des contenus et les classer dans la section "Séries" et aussi sélectioner la série auquel le contenu appartient. Voir l'exemple plus bas.;
4. Seul les séries avec l'option *"Serie vedette"* se retrouvent dans l'entête de l'accueil de la section *Séries*;

Détail **important** ! Une série avec _aucun_ article publié ne sera **pas** visible sur le site tant et aussi longtemps qu'au moins 1 article de la série n'est publié.

![](https://assets.oui.surf/wp-content/uploads/2017/10/03174513/skitch-6.png)

**Note :** vous pouvez attacher les contenus de séries à la section "Vidéos" aussi (tel illustré sur la capture ci-dessus), ainsi le vidéo sera visible à partir des deux contextes sur le site, *Séries* et *Vidéos*.

### Ordre des séries

Vous pouvez chagner l'ordre d'affichage des séries avec l'outil suivant, [Taxonomy order](https://oui.surf/wp-admin/edit.php?page=to-interface-post&taxonomy=series). En général la série plus récente devrait se retrouver en premier, mais parfois non. La flexibilité est là, et par default votre nouvelle catégorie de série ne sera pas en premier dans la liste.

### Champs spéciaux pour les vidéos

Les contenus séries et vidéos ont des champs spéciaux pour le lecteur Vimeo/Youtube, référez-vous aux rubriques d'aide pour chacun des champs.

![](https://assets.oui.surf/wp-content/uploads/2017/10/03174534/skitch-3.png)

### Musique d'une série

Pour ajouter la musique associer à une série, vous devez le faire par épisode, et non à la catégories *Série* associée. Dès que le billet WP est classé dans une *Série*, ce tableau d'entrée de données sera visible :

![](https://assets.oui.surf/wp-content/uploads/2017/10/03175157/skitch-4.png)

## Dossiers

Les dossiers sont sensiblement la même chose que les articles textes réguliers, l'unique différence est que vous pouvez attacher des articles lié à un guide.

D'abord, classez votre contenu dans la section "Guides".

![](https://assets.oui.surf/wp-content/uploads/2017/10/04014439/skitch-17.png)

Ensuite l'outil de sélection d'articles reliés sera visible au bas de la page.

![](https://assets.oui.surf/wp-content/uploads/2017/10/04014443/skitch-16.png)

Article exemple : https://oui.surf/bali-lombok/

## Publicités

La gestion des publicités sur le site se fait avec l'outil [AdRotate](https://oui.surf/wp-admin/admin.php?page=adrotate-ads).

Il faut en premier temps créer des publicité et les assigner au groupe "Big box rotation". Il faut choisir une date de début et fin, nombre d'impressions maximum, etc. etc.

![](https://assets.oui.surf/wp-content/uploads/2017/10/10154044/skitch-23.png)

Ces publicités sont placées un peu partout dans l'interface du site avec l'outil [Widgets](https://oui.surf/wp-admin/widgets.php) de WP.

Consultez la [documentation](https://ajdg.solutions/manuals/adrotate-manuals/) officielle pour bien comprendre les fonctions de bases et avancées de AdRotate.

https://ajdg.solutions/manuals/adrotate-manuals/


## Outil de mise en page

Il est possible de faire des mises en page avancé avec l'outil "_longform_" du CMS, voici les grandes lignes.

> D'aboard, il faut comprendre le principle d'un système de grille à **12 colonnes**. C'est un peu comme un journal papier et ses colonnes de textes. Voici un exemple de système d'un grille à 12 colonnes.

![](https://developers.google.com/web/updates/images/2017/01/css-grid/examplelayout.png)

À noter qu'il y a un espace de quelques pixels entre chaques colonnes, mieux connu sous le nom de "gutter" en édition.

Les éléments peuvent utiliser une ou plusieurs colonnes pour se retrouver côte à côte. Il faut toujours calculer un total de 12 par rangées.

Donc, pour une rangée *3 photos*. Les contenus auront *4 unités* de colonnes chacune.

![](https://assets.oui.surf/wp-content/uploads/2017/10/03191641/skitch-14.png)

> 3 éléments x 4 rangées = **12 colonnes**

Voici comment la mise en page ci-dessus est configuré dans le CMS. On apperçoit que le premier des 3 éléments de l'exemple, mais notez bien le paramêtre qui détermine le nombre de colonnes pour cette image.

![](https://assets.oui.surf/wp-content/uploads/2017/10/03193146/skitch-15.png)

Vous pouvez varier sur les combinaisons, tant que le total s'aditionne à 12.

* 2 éléments de 3 *colonnes* + 1 élément de 6 *colonnes* = 12
* 2 éléments de 6 *colonnes* = 12
* 4 éléments de 3 *colonnes* = 12

**Important :** Après 12 éléments, une nouvelle rangée va automatiquement s'ajouter. Donc vous recommencer à 0 le total de colonnes.

Exemple de rangées qui se suivent :

1. 2 X 6 colonnes = nouvelle rangée
2. 4 X 3 colonnes = nouvelle rangée
3. 1 X 12 colonnes = nouvelle rangée
4. 3 X 4 colonnes = nouvelle rangée

### Type d'éléments visuels / texte

Ce système de grille est la base pour la mise en page, ensuite il est possible d'y mettre différents types de contenus, avec des sous-variations :

* Texte
* Image inline (pas un background)
* Image en background
* Gallerie d'image
* Video background en lecture automatique
* Billets vidéos

À ceci s'ajoute quelques options de mise en page, exemple les "image en background" peuvent être fixes avec la barre de défilement pour donner un effet intéressant. Ou encore vous pouvez ajouter des effets de d'apparition des éléments (fadeIn, fadeInUp, fadeInLeft, etc).

### Tutoriel vidéo

Pour une démonstration complète de l'outil, visionner le **vidéo suivant**, avec le son et en plein écran pour bien suivre.

<iframe width="560" height="315" src="https://www.youtube.com/embed/ZRRZkwKSPp4" frameborder="0" allowfullscreen></iframe>

# 😎 🏄 😎 🏄 😎 🏄 😎 🏄 😎 🏄
