// config
exports.config = {
  app_name: ['aide_ouisurf'],
  license_key: process.env.NEW_RELIC_LICENSE_KEY,
  logging: {
    level: process.env.NEW_RELIC_LOG
  }
}
